{-
-- | Lógica Computacional 2022-01
-- | Práctica 2: Forma normal negativa y conjuntiva
-- | Profesor: Dr. Favio E. Miranda Perea
-- | Ayudante: Javier Enríquez Mendoza
-- | Ayudante: Ramón Arenas Ayala
-- | Laboratorio: Daniel Lugo Cano
-}

module Practica2 where

import LProp

-- |1| Funcion fnn que recibe una formula φ y devuelve su forma normal negativa
fnn :: Prop -> Prop
fnn Top = Top 
fnn Bot = Bot
fnn (P x) = (P x)
fnn (Or p1 p2) = (Or (fnn p1) (fnn p2))
fnn (And p1 p2) = (And (fnn p1) (fnn p2))
fnn (Impl p1 p2) = fnn (elimImp (Impl p1 p2))
fnn (Syss p1 p2) = fnn (elimEquiv (Syss p1 p2))
fnn (Neg p) = case p of
  Top -> Bot
  Bot -> Top
  P x -> P x
  (Neg x) -> fnn x
  (Or p1 p2) -> (And (fnn (Neg p1)) (fnn (Neg p2)))
  (And p1 p2) -> (Or (fnn (Neg p1)) (fnn (Neg p2)))
  (Impl p1 p2) -> fnn (Neg(elimImp (Impl p1 p2)))
  (Syss p1 p2) -> fnn (Neg(elimEquiv (Syss p1 p2)))
--
--
-- |2| Funcion distr la cual aplica adecuadamente las leyes distributivas a una formula proposicional
distr :: Prop -> Prop
distr (Or (And p q) (And r s)) = (And (And (Or (distr p) (distr r)) (Or (distr p) (distr s))) (And (Or (distr q) (distr r))(Or (distr q) (distr s)))) 
distr Bot = Bot
distr Top = Top
distr (P x) = (P x)
distr (Neg x) = Neg (distr x)
--
distr (Or p1 (And p2 p3)) = (And (Or (distr p1) (distr p2)) (Or (distr p1) (distr p3)))
distr (Or (And p1 p2) p3) = (And (Or (distr p1) (distr p3)) (Or (distr p2) (distr p3)))
--
distr (And p1 (Or p2 p3)) = (Or (And (distr p1) (distr p2)) (And (distr p1) (distr p3)))
distr (And (Or p1 p2) p3) = (Or (And (distr p1) (distr p3)) (And (distr p2) (distr p3)))
--
distr (Or p1 p2) = (Or (distr p1) (distr p2))
distr (And p1 p2) = (And (distr p1) (distr p2))
--
--
-- |3| Funcion fnc que recibe una formula φ y devuelve su forma normal conjuntiva, es decir:
--     Permite expresar cualquier formula proposicional como una conjunción de disyunciones.
-- IMPORTANTE: Se puede suponer que la formula de entrada ya se encuentra en forma normal negativa
fnc :: Prop -> Prop   
fnc Top = Top
fnc Bot = Bot
fnc (P x) = (P x)
fnc (Neg x) = (Neg x)
fnc (And p1 p2)= (And (fnc p1)(fnc p2))
fnc (Or p1 p2)= distr (Or (fnc p1) (fnc p2))
--
--PRUEBA DE ORO:    fnc (Or (And (P 'p') (P 'q')) (And (P 'r') (P 's')))
--
--Definimos un tipo de dato Literal como sinónimo de fórmula proposicional
type Literal = Prop
--
--Definimos un tipo de dato Clausula como una lista de literales
type Clausula = [Literal]
--
--
-- |4| Función ctolist recibe una proposicion y devuelve la forma clausular de la proposicion
ctolist :: Prop -> Clausula
ctolist Bot = [Bot]
ctolist Top = [Top]
ctolist (P x) = [P x]
ctolist (Neg x) = [Neg x]
ctolist (And a b) = [And a b]
ctolist (Or a b) = rmD ((ctolist a) ++ (ctolist b))
ctolist (Impl a b) = [Impl a b]
ctolist (Syss a b) = [Syss a b]
--pruebas
-- ctolist (Neg (P 'p')) = [¬ 'p']
--
-- Funcion que divide una lista en cabeza y cola, para mandarla a rmDaux
rmD ::  Clausula -> Clausula
rmD [] = []
rmD (a:ab) = [a] ++ (rmDaux [a] ab)
--
-- Funcion que elimina los elmentos repeditos
rmDaux :: Clausula -> Clausula -> Clausula
rmDaux [] a = []
rmDaux [s] (a:ab) = if s==a
                    then (rmDaux [s] ab)
                    else ([s] ++ (rmDaux [a] ab))
--
--
--
-- |5| Función fncC recibe una fórmula en forma normal conjuntiva y debe devolver su conversión en una lista de cláusulas.
fncC :: Prop -> [Clausula]
fncC Bot = [ctolist Bot]
fncC Top = [ctolist Top]
fncC (P x) = [ctolist (P x)]
fncC (Neg (P x)) = [ctolist (Neg (P x))]
fncC (Or x y)  = if (Or x y) == (fnc (Or x y))
                   then [ctolist (Or x y)]
                   else [[Or x y]]
fncC (And x y) = if ((fnc y)==y) && ((fnc x)==x) 
                   then (fncC x) ++ (fncC y)
                   else [[And x y]]
fncC (Syss x y) = [[Syss x y]]
fncC (Impl x y) = [[Impl x y]]
--
--
-- |6| Funcion fncConj recibe una proposicion y devuelve el conjunto de clausulas equivalentes a esa proposicion
fncConj :: Prop -> [Clausula]
fncConj prop = fncC (fnc (fnn prop))



