Práctica 02: Forma Normal Negativa y Conjuntiva
===================================

Logica Computacional
--------------------

### Integrantes

* Jean Durán Villanueva
* Julio Vázquez Alvarez
* Sebastian Ulises Torres Aduna

### Desarrollo
La práctica se desarrollo dejando a cada uno de los integrantes dos funciones a
resolver. En ellas se dió un _feedback_ entre todos los integrantes junto con 
pruebas exhaustivas para verificar la correctez de la función.

Cuando los tres integrantes estuvimos de acuerdo con las pruebas individuales 
que cada quieén realizo la función se agregó a un archivo general .hs para su
futura entrega.

### Dificultades
