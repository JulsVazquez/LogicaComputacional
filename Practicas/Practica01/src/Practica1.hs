{-
-- | Lógica Computacional 2022-01
-- | Práctica 1: Introducción a Haskell 
-- | Profesor: Dr. Favio E. Miranda Perea
-- | Ayudante: Javier Enríquez Mendoza
-- | Ayudante: Ramón Arenas Ayala
-- | Laboratorio: Daniel Lugo Cano
-}

module Practica1 where 

-- | Tipo de dato Binario, es la representacion de todos los numero binarios que empiezan con uno
data Binario = U | Cero Binario | Uno Binario 

-- |1| Definicion de la clase Show para el tipo de dato Binario
instance Show Binario where
    show (U) = "1"
    show (Cero p) = "" ++ show p ++ "0"
    show (Uno p) = "" ++ show p ++ "1"

-- |2| sucesor. Regresa el sucesor de un Binario
-- -> Ejemplo sucesor de U (uno)  = Cero U , sucesor de 1 es 10
sucesor  :: Binario -> Binario
sucesor U = Cero U
sucesor (Cero p) = Uno p
sucesor (Uno p) = Cero (sucesor p)

-- |3| suma. Regresa la suma de 2 numeros de un Binarios
-- -> ejemplo suma de U U = Cero U , suma de 1 y 1 es 10
suma :: Binario -> Binario -> Binario
suma U U = sucesor (U)
suma (U) (Cero p) = sucesor (Cero p)
suma (U) (Uno p) = sucesor (Uno p)
suma (Cero p) (U) = sucesor (Cero p)
suma (Uno p) (U) = sucesor (Uno p)
suma (Cero p) (Cero q) = Cero (suma p q)
suma (Cero p) (Uno q) = Uno (suma p q)
suma (Uno p) (Cero q) = Uno (suma p q)
suma (Uno p) (Uno q) =  sucesor (Uno (suma p q))

-- |4| producto. Regresa el producto de 2 numeros Binarios
-- -> Ejemplo producto de (Cero U) (Cero U) = (Cero (Cero U)) , producto de 10 con 10 es 100
producto :: Binario -> Binario -> Binario
producto x y = natABin((binANat(x) * binANat(y))) 

-- |5| natBinLista. Regresa el la representacion en Binario de un numero Decimal en forma de Lista
-- -> ejemplo natBinLista 8 = [1,0,0,0]
natBinLista :: Int -> [Int]
natBinLista 0 = [0]
natBinLista n = reversa(natBinListaAux(n))

-- Función auxiliar reversa
reversa :: [Int] -> [Int]
reversa (x:xs) = (reversa xs) ++ [x]
reversa [] = []

-- Funcion auxilliar natBinListAux
natBinListaAux :: Int -> [Int]
natBinListaAux 0 = []
natBinListaAux n = if(n `mod` 2 ==  1)
                   then 1 : natBinListaAux(n `div` 2)
                   else 0 : natBinListaAux(n `div` 2)

-- |6| sumaBinLista. Regresa la suma de 2 listas que representan 2 numeros binarios.
-- -> ejemplo sumaBinLista de [1] [1,0] = (Uno U)
sumaBinLista :: [Int] -> [Int] -> Binario
sumaBinLista [] [] = error "lista vacia"
sumaBinLista a b = suma (listaABinarioAux a) (listaABinarioAux b)

-- listaABinario. Toma una lista de enteros que representan un numero binario. Regresa la representacion en Binario.
-- -> ejemplo listaABinario de [1,0,0,1,1] = Uno(Uno(Cero(Cero U)))
listaABinario :: [Int] -> Binario
listaABinario [] = error "La lista es vacía" 
listaABinario (x:xs) = if(length xs == 0) 
                       then U
                       else if(x==0)
                       then Cero(listaABinario xs)
                       else if(x==1)
                       then Uno(listaABinario xs)
                       else error "Algún número no es ni 0 ni 1"

-- Funcion auxiliar listaABinarioAux
listaABinarioAux :: [Int] -> Binario
listaABinarioAux [] = error "La lista es vacía"                                        
listaABinarioAux (1:xs) = listaABinario (reversa (1:xs))
listaABinarioAux (_:xs) = error "No inicia con 1, bro"


{-- PUNTOS EXTRA --}

-- |1| natABin: Recibe un natural (mayor que 1) y devuelve un binario
natABin :: Int -> Binario
natABin 0 = error "Debe ser mayor a 0, bro"
natABin x = (listaABinarioAux (reversa (natBinLista x)))

-- |2| binANat: Recibe un binario y devuelve su reprentacion en numero natural (mayor que 1).
binANat :: Binario -> Int
binANat x = (binANatAux x 0)

-- Función auxiliar binANat
binANatAux :: Binario -> Int -> Int
binANatAux U x = 2^x
binANatAux (Cero p) x = binANatAux p (x+1)
binANatAux (Uno p) x = (2^x) + (binANatAux p (x+1))

-- |3| predecesor: Recibe un binario y devuelve su binario anterior
predecesor :: Binario -> Binario
predecesor U = U
predecesor (Cero U) = U
predecesor (Cero p) = Uno (predecesor p)
predecesor (Uno p) = Cero p
