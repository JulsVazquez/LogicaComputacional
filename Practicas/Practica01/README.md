Práctica 01: Introducción a Haskell
===================================

Logica Computacional
--------------------

### Integrantes

* Jean Durán Villanueva
* Julio Vázquez Alvarez
* Sebastian Ulises Torres Aduna

### Desarrollo
La práctica se desarrollo dejando a cada uno de los integrantes dos funciones a
resolver. En ellas se dió un _feedback_ entre todos los integrantes junto con 
pruebas exhaustivas para verificar la correctez de la función.

Cuando los tres integrantes estuvimos de acuerdo con las pruebas individuales 
que cada quieén realizo la función se agregó a un archivo general .hs para su
futura entrega.

### Dificultades
La mayoría de las dificultades se sucitaron al ser los tres nuevos en el 
lenguaje Haskell, ya que ninguno había tenido una interacción sería con el 
mismo.

Dentro de la práctica existió mucho conflicto acerca de todos los casos base de
las funciones. Una de las que podemos decir explicitamente son las funciones
_suma_ y _sumaBinLista_. 

Recurrimos al ayudante para resolver nuestras dudas y gracias a él ambas 
funciones lograron compilar y trabajar de manera correcta.