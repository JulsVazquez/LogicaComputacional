{-
-- | Lógica Computacional 2022-01
-- | Práctica 4: Unificación
-- | Profesor: Dr. Favio E. Miranda Perea
-- | Ayudante: Javier Enríquez Mendoza
-- | Ayudante: Ramón Arenas Ayala
-- | Laboratorio: Daniel Lugo Cano
-}

module Practica4 where

import LPO
import LPOSintac

-- |1| Funcion simpSus que dada una sustitución elimina de ella los pares con componentes iguales
simpSus :: Subst -> Subst
simpSus sub = filter (\(name, term)-> term /= (V name)) sub

-- |2| Funcion compSus la cual recibe dos Subst y devuelve su compisición.
compSus :: Subst -> Subst -> Subst
-- obtener cada simpSus
-- componer de primera subst, la segunda subst
-- simpSus de la composicion
-- agregar las subst que no aparecen en la primera
compSus sust1 sust2 = s1_comp ++ s2_filter
  where
    s2_filter = filter (\ (term, sust) -> not (term `elem` s1_vars)) s2
    s1_vars = map (\ (term, sust) -> term) s1
    s1_comp = simpSus (map (\ (term, sust) -> (term, apSubT sust s2)) sust1)
    s1 = simpSus sust1
    s2 = simpSus sust2

-- |3| Funcion que dados dos términos devuelva una lista de sustituciones que cumplan las condiciones dadas
substituteAndUnify :: [Subst] ->(Term , Term) -> [Subst]
substituteAndUnify [] (p,q) = []
substituteAndUnify [s] (p,q) = if possibleNewSub == []
                          then []
                          else [compSus s newSub]
                          where possibleNewSub = unifica (apSubT p s) (apSubT q s)
                                newSub = head possibleNewSub
unifica :: Term -> Term -> [Subst]
unifica (F n1 params1) (F n2 params2)
    | n1 /= n2 = [] --DFALLA
    | otherwise = foldl (substituteAndUnify) [[]] $zip params1 params2 --DESC
unifica (F n1 params) (V n2) = unifica (V n2) (F n1 params) --SWAP
unifica (V n1) t
    | (V n1) == t = [[]] --ELIM
    | elem n1 (varT t) = [] --SFALLA
    | otherwise = [[(n1, t)]] --SUST

-- |4| Funcion que unifica dos listas de términos de la misma longitud componente a componente
unificaListas :: [Term] -> [Term] -> [Subst]
unificaListas l1 l2 = foldl (++) [] tuplas_terms
    where
      tuplas_terms = map (\ (term1, term2) -> unifica term1 term2) (zip l1 l2)

-- |5| Funcion unificaConj que implementa el caso general para unificar un conjunto (lista)
unificaConj :: [Term] -> [Subst]
unificaConj xs = unificaConj_aux xs []

unificaConj_aux :: [Term] -> [Subst] -> [Subst]
unificaConj_aux (x:(y:ys)) s =
   if uni == [[]]
   then error "No se puede unificar"
   else
     if ys == []
     then s ++ uni
     else do
       unificaConj_aux (sustituir x uni ++ ys) (s ++ uni)

   where
     uni = unifica x y

-- Función que realiza la sustitución en un termino.
sustituir :: Term -> [Subst] -> [Term]
sustituir x (y:ys) =
  if ys == []
  then [apSubTaux newX y]
  else
    sustituir newX ys

  where
    newX = apSubTaux x y

-- |6| Funcion que inifica dos literales
unificaLit :: Form -> Form -> [Subst]
unificaLit (Pr n1 params1) (Pr n2 params2) =
    if n1 /= n2 || uni == []
    then error "No unificables"
    else uni

    where
          uni = unificaConj (params1 ++ params2)

-- |E1 helper 1| Función que devuelve la lista con los elementos discordes si son diferentes.
-- Si son iguales hace recursión o devuelve la lista vacía
terminoDiscorde :: Term -> Term -> (Either [Form] [Term])
terminoDiscorde (V x) (F y ys) = Right [(V x), (F y ys)]
terminoDiscorde (F y ys) (V x) = Right [(F y ys), (V x)]
terminoDiscorde (V x) (V y) =
  if x == y
  then Right []
  else Right [(V x), (V y)]
terminoDiscorde (F x xs) (F y ys) =
  if x == y
  then listaTermDiscorde xs ys
  else Right [(F x xs), (F y ys)]

-- |E1 helper 2| Función que devuelve los primeros dos términos que difieren. Si son iguales devuelve la lista vacía
listaTermDiscorde :: [Term] -> [Term] -> (Either [Form] [Term])
listaTermDiscorde [] xs = Right xs
listaTermDiscorde xs [] = Right xs
listaTermDiscorde ((F x xterms):xs) ((V y):ys) = Right [(F x xterms), (V y)]
listaTermDiscorde ((V y):ys) ((F x xterms):xs) = Right [(V y), (F x xterms)]
listaTermDiscorde ((V x):xs) ((V y):ys) =
	if x == y
	then listaTermDiscorde xs ys
	else Right [(V x), (V y)]
listaTermDiscorde ((F x xterms):xs) ((F y yterms):ys) =
	if x == y
	then
		if primerElemDiscorde == Right []
		then listaTermDiscorde xs ys
		else primerElemDiscorde
	else Right [(F x xterms), (F y yterms)]
	where
		primerElemDiscorde = terminoDiscorde (F x xs) (F y ys)

-- |E1 helper helper 3| Función que devuelve la lista de predicado de una fórmula [(Pr x xterms)]
listaPredicados :: Form -> [Form]
listaPredicados TrueF = [TrueF]
listaPredicados FalseF = [FalseF]
listaPredicados (Pr x xterms) = [(Pr x xterms)]
listaPredicados (Eq t1 t2) = [(Eq t1 t2)]
listaPredicados (Neg x) = listaPredicados x
listaPredicados (Conj x y) = (listaPredicados x) ++ (listaPredicados y)
listaPredicados (Disy x y) = (listaPredicados x) ++ (listaPredicados y)
listaPredicados (Imp x y) = (listaPredicados x) ++ (listaPredicados y)
listaPredicados (Equi x y) = (listaPredicados x) ++ (listaPredicados y)
listaPredicados (All x xform) = listaPredicados xform
listaPredicados (Ex x xform) = listaPredicados xform


-- |E1 helper 4| Función que aplica conjuntoDiscorde a dos listas de fórmulas, ya predicados
-- y repite esto por cada índice hasta encontrar una diferencia
aplicaConjuntoDiscorde :: [Form] -> [Form] -> (Either [Form] [Term])
aplicaConjuntoDiscorde [] [] = Right []
aplicaConjuntoDiscorde [] xs = Left xs
aplicaConjuntoDiscorde xs [] = Left xs
aplicaConjuntoDiscorde (x:xs) (y:ys) =
	if discordes == Left [] || discordes == Right []
	then aplicaConjuntoDiscorde xs ys
	else discordes
	where
	discordes = conjuntoDiscorde x y

-- |E1| Función que devuelve los primeros dos términos que difieren. De lo contrario devuelve la lista con los predicados.
conjuntoDiscorde :: Form -> Form -> (Either [Form] [Term])
conjuntoDiscorde TrueF TrueF = Left []
conjuntoDiscorde FalseF FalseF = Left []
conjuntoDiscorde TrueF FalseF = Left [TrueF, FalseF]
conjuntoDiscorde FalseF TrueF = Left [FalseF, TrueF]
conjuntoDiscorde TrueF x = Left [TrueF, x]
conjuntoDiscorde FalseF x = Left [FalseF, x]
conjuntoDiscorde x TrueF = Left [x, TrueF]
conjuntoDiscorde x FalseF = Left [x, FalseF]
conjuntoDiscorde (Pr x termsx) (Pr y termsy) =
	if x == y
	then listaTermDiscorde termsx termsy
	else Left [(Pr x termsx), (Pr y termsy)]
conjuntoDiscorde x y = aplicaConjuntoDiscorde xpreds ypreds
	where
		xpreds = listaPredicados x
		ypreds = listaPredicados y
