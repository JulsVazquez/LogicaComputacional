module DPLL where

import LProp
import Data.List

-- Importen los módulos necesarios de las prácticas anteriores para tener sus definiciones
-- de lógica proposicional

type Literal = Prop
type Clausula = [Literal]
type Formula = [Clausula]
type Modelo = [Literal]
type Configuracion = (Modelo,Formula)

elimdup_aux :: (Eq a) => [a] -> [a] -> [a]
elimdup_aux [] x = x
elimdup_aux (x:xs) ys =
	if x `elem` ys
	then elimdup_aux xs ys
	else elimdup_aux xs (x:ys)

elim_dup :: (Eq a) => [a] -> [a]
elim_dup x = elimdup_aux x []


--------------------------------------  BÚSQUEDA DE MÓDELOS HACIA ATRAS ------------------------------
single_clause :: Clausula -> Clausula
single_clause [x] = [x]
single_clause _ = []

single_clauseF :: Formula -> Clausula
single_clauseF [] = []
single_clauseF (x:xs) =
  if clause == []
  then single_clauseF xs
  else clause
  where
    clause = single_clause x


unit :: Configuracion -> Configuracion
unit (m,f) =
  if literal == []
  then (m,f)
  else do
    if (head literal `elem` m)
    then unit (m,delete literal f)
    else unit (m ++ literal,delete literal f)

  where literal = single_clauseF f

elim_aux :: Literal -> Formula -> Formula
elim_aux l (f:fs) =
  if l `elem` f
  then elim_aux l fs
	else [f] ++ elim_aux l fs

elim_aux l _ = []

elim_aux2 :: Configuracion -> Formula
elim_aux2 ((m:ms),f) =  elim_aux2 (ms,elim_aux m f)
elim_aux2 ([],f) = []


elim :: Configuracion -> Configuracion
elim ((m:ms),f) = ((m:ms),elim_aux2 ((m:ms),f))



is_complementary :: Prop -> Prop -> Bool
--assuming Prop is a literal
is_complementary (Neg p) q = q == p
is_complementary q (Neg p) = q == p
is_complementary p q = False

complement_present :: Modelo -> Literal -> Bool
complement_present m l = or $ map (is_complementary l) m

red :: Configuracion -> Configuracion
red (m,f) = (m, result)
    where is_not_reduction_candidate = (\l -> not $ complement_present m l)
          result = map (\c -> filter is_not_reduction_candidate c) f

vars_clausula :: Clausula -> [VarP]
vars_clausula c = elim_dup (foldl (++) [] list_vars)
	where list_vars = map (vars) c

vars_formula :: Formula -> [VarP]
vars_formula f = elim_dup (foldl (++) [] list_vars)
	where list_vars = map (vars_clausula) f

split :: Configuracion -> [Configuracion]
split (m, []) = [(m, [])]
split (m,form) =
	if vars_form == []
	then do
		[(m,form)]
	else do
		let mivar = head vars_form
		[(((P mivar):m),form), (((Neg (P mivar)):m),form)]
	where
		vars_form = [x | x<-vars_in_m, not ((P x) `elem` m)] -- filter
		vars_in_m = vars_formula form

conflict :: Configuracion -> Bool
conflict (_, [[]]) = True
conflict _ = False

success :: Configuracion -> Bool
success (m , []) = True
success (m, f) = False

----------------------------------------------- ARBOLES DPLL -------------------------------------

elim_aux_rcu :: Literal -> Formula -> Formula
elim_aux_rcu l (f:fs) =
	if l `elem` f
	then elim_aux_rcu l [delete l f] ++ fs
	else [f] ++ elim_aux_rcu l fs

elim_aux_rcu l _ = []

rcu :: Formula -> Formula
rcu (f:fs) =
  if l == []
	then (f:fs)
	else do
		rcu(elim_aux_rcu (Neg (head l)) (delete l (f:fs)))
	where
		l = single_clauseF(f:fs)


rlp :: Formula -> Formula
rlp fs =
  if pure == Bot
	then fs
	else elim_aux pure fs
	where
		pure = checkForm fs fs

checkForm :: Formula -> Formula -> Literal
checkForm [] _ = Bot
checkForm (f:fs) fc =
  if checkClau f fs == Bot
	then checkForm fs fc
	else checkClau f fs

checkClau :: Clausula -> Formula -> Literal
checkClau [] f = Bot
checkClau (x:xs) f =
  if check x f
	then checkClau xs f
	else x


check:: Literal -> Formula -> Bool
check _ [] = False
check l (f:fs) =
  if l `elem` f
	then True
	else check l fs

rd :: Formula -> (Formula,Formula)
rd f = (positiva++neutra, negativa++neutra)
	where
		list_vars = vars_formula f
		mivar = P (head list_vars)
		positiva = filter (\c -> mivar `elem` c) f
		negativa = filter (\c -> (Neg mivar) `elem` c) f
		neutra = filter (\c -> not (mivar `elem` c || (Neg mivar) `elem` c)) f
		(_,s) = elim ([mivar], positiva)
		(_,sp) = elim ([Neg mivar], negativa)

data ArbolDPLL =
                 Void
               | Uni Formula ArbolDPLL
               | Bi Formula ArbolDPLL ArbolDPLL
               deriving (Eq, Show)


can_rlp :: Formula -> Bool
can_rlp f = or $map (\l -> not $ complement_present literal_set l) literal_set
    where literal_set = (nub (foldl (++) [] f))


dplltree :: Formula -> ArbolDPLL
dplltree f
         | f == [] || [] `elem` f = Void
         | length singletons > 0 = Uni f (dplltree (rcu f))
         | can_rlp f= Uni f (dplltree (rlp f))
         | otherwise = Bi f (dplltree $ rd_res1) (dplltree $ rd_res2)
         --strongly depending on haskell's lazyness
         where singletons = filter (\c -> length c == 1) f
               (rd_res1, rd_res2) = rd f
