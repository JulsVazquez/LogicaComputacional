module LaboratorioIntroduccion where

areaCirculo :: (Num a, Fractional a) => a -> a 
areaCirculo radio = (3.141592*radio^2)

areaTriangulo :: (Num a, Fractional a) => a -> a -> a 
areaTriangulo base altura = (base * altura) / 2

par :: Int -> Bool
par numero = if (numero `mod` 2 == 0)
                then True
                else False

-- Creando la reversa de una lista inferiendo tipos.
reversa :: [a] -> [a]
reversa [] = []
reversa (x:xs) = (reversa xs) ++ [x]

{- 
    Creando nuestras propias datas.
    deriving Eq : Es una palabra reservada para comparar datos en Haskell.
-}

data Proposicion = Falso
                 | Verdadero
                 | Variable Var 
                 | Neg Proposicion
                 | Con Proposicion Proposicion
                 | Dis Proposicion Proposicion
                 | Imp Proposicion Proposicion
                 | Syss Proposicion Proposicion deriving Eq


type Var = Char

type Estado = [Var]

-- Definimos una lista por comprensión en Haskell.
abc = [x | x <- ['A'..'Z']]

tomaVar :: Var -> [Char] -> Var
tomaVar _ [] = error "No se encuentra el caracter"
tomaVar p (x:xs) = if (p == x)
                   then x
                   else tomaVar p xs 

-- Para que se vea bonito en Haskell, entonces usamos instance Show P
instance Show Proposicion where
    -- Mostramos como se verá nuestra variable en Haskell
    show (Variable p) = "(" ++ show p ++ ")"
    -- Mostramos como se verá la negación.
    show (Neg p) = "(¬" ++ show p ++ ")"
    -- Mostramos como se verá la conjunción.
    show (Con p q) = "(" ++ show p ++ "&" ++ show q ++ ")"
    -- Mostramos como se verá la disyunción.
    show (Dis p q) = "(" ++ show p ++ "|" ++ show q ++ ")"
    -- Mostramos como se verá la implicación
    show (Imp p q) = "(" ++ show p ++ "->" ++ show q ++ ")"
    -- Mostramos como se verá el sí y sólo si
    show (Syss p q) = "(" ++ show p ++ "<->" ++ show q ++ ")"
    -- Falso
    show (Falso) = "FALSO"
    -- Verdadero
    show (Verdadero) = "VERDADERO"

-- La sentencia case nos ayuda para elegir alguna de las dos variables
{-
interpretacion :: Proposicion -> Estado -> Bool
interpretacion prop est = case prop of 
    Falso -> False
    Verdadero -> True
    Variable var -> (inter (Variable var) est)
    (Neg p) -> (not(interpretacion p est))
    (Con p q) -> (interpretacion p est) && (interpretacion q est)
    (Dis p q) -> (interpretacion p est) || (interpretacion q est)
    (Imp p q) -> (not(interpretacion p est)) || (interpretacion q est)
    (Syss p q) ->  ((not(interpretacion p est)) || (interpretacion q est)) && ((not(interpretacion q est)) || (interpretacion p est))
-}

-- Hacemos una función auxiliar que nos ayude
inter :: Proposicion -> Estado -> Bool
inter (Variable var) [] = False
inter (Variable var) (x:xs) = if(var == x)
                              then True
                              else inter(Variable var ) xs


